from django.db import models

# Create your models here.
class Reg(models.Model):
    name=models.CharField(max_length=15)
    email=models.CharField(max_length=20)
    ph_no=models.CharField(max_length=10)
    degree=models.CharField(max_length=10)
    password=models.CharField(max_length=10)

    def __str__(self):
        return self.name
